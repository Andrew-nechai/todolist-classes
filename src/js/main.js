class CopyBlock {
    constructor(id_element){
        this.srccheckepath = "dist/img/checked.svg";
        this.srcuncheckepath = "dist/img/unchecked.svg";
        
        this.idelem = id_element.id;

        this.$input_line = id_element.querySelector(".todolist__input");

        this.image_checkbox_src_check = "dist\\img\\checked.svg";
        this.image_checkbox_src_uncheck = "dist\\img\\unchecked.svg";
        this.image_delete_deal_src = "dist\\img\\delete.svg";
        
        this.$content_block = id_element.querySelector(".todolist__content");
        this.$count_block = id_element.querySelector(".todolist__count-uncheck-deals");
        this.$count_block.innerText = "Unchecked tasks: ";

        this.deals_mass = [];

        this.$tool_block = id_element.querySelector(".todolist__toolbuttons");
		this.$tool_block_button_all = id_element.querySelector(".todolist__toolbuttons-select-all");
		this.$tool_block_button_all.classList.add("unselected-toolbutton");
		this.$tool_block_button_active = id_element.querySelector(".todolist__toolbuttons-select-active");
		this.$tool_block_button_active.classList.add("unselected-toolbutton");
		this.$tool_block_button_complete = id_element.querySelector(".todolist__toolbuttons-select-complete");
		this.$tool_block_button_complete.classList.add("unselected-toolbutton");
		this.$tool_block_button_clear =id_element.querySelector(".todolist__toolbuttons-delete");

        //добавление дела через Enter
        this.$input_line.onkeyup = this.btnClickAdd.bind(this);

        //Взятие данных из localeStorage, их обработка при обновлении страницы
        this.unloadFunction(id_element);

        //выделение нужных дел кнопками
        this.$tool_block_button_all.addEventListener('click', this.selectAllDeals.bind(this));
        this.$tool_block_button_active.addEventListener('click', this.selectActiveDeals.bind(this));
        this.$tool_block_button_complete.addEventListener('click', this.selectCompleteDeals.bind(this));
        this.$tool_block_button_clear.addEventListener('click', this.clickOnButtonClearItems.bind(this));
    }

    //методы

    //Функция удаления дел из массива и перезапись localStorage
    deleteDeals() {
        let i = 0;
        while(this.deals_mass[i]){
            if (this.deals_mass[i].existance == false){
                this.deals_mass.splice(i,1);
                i--;
            }
            i++;
        }
        localStorage.setItem(this.idelem + "", JSON.stringify(this.deals_mass));
    }


    //Взятие элементов из localStorage
    unloadFunction(id_element) {
		let i = 0;
		if (localStorage.getItem(id_element.id) != null){
			this.deals_mass = JSON.parse(localStorage.getItem(id_element.id));
		} else {
			return;
		}

		while(this.deals_mass[i]){
			let item_add = this.deals_mass[i];
			this.createDeal(item_add, false);		
			i++;
		}
		this.countUncheckDeals();
	}

    btnClickAdd(key) {
        if (key.keyCode == 13) this.addDeal();
    }

    addDeal() {
        console.log(this.deals_mass)
		if (this.$input_line.value == ""){
			return
		}
		let item_add = {
			task: this.$input_line.value, 
			check: "unchecked", 
			existance: true
		}

		this.removeShadow();
		this.resetToolbuttonsStyle();
		this.createDeal(item_add);
		this.countUncheckDeals();

		// добавление дела в массив и в localStorage
		this.deals_mass.push(item_add);
		localStorage.setItem(this.idelem + "", JSON.stringify(this.deals_mass))


		//очистка input
		this.$input_line.value = "";
    }
    
    removeShadow() {
        let i = 0;

        //КОСТЫЛЬ

        while (this.$content_block.childNodes[i] && this.$content_block.childNodes[i].tagName){
            if (this.$content_block.childNodes[i].classList.contains("red-boxshadow")){
				this.$content_block.childNodes[i].classList.toggle("red-boxshadow");
				this.$content_block.childNodes[i].classList.toggle("shadow");
            }
            i++;
        }
    }
    
    createDeal(item_add, animate) {

		//создание дела
		let deal 			   = document.createElement("div");
		let button_checkbox    = document.createElement("div");
		let image_checkbox     = document.createElement('img');
		let input_content      = document.createElement("div");
		let button_delete_deal = document.createElement("div");
		let image_delete_deal  = document.createElement('img');

		//стилизация и объединение
		deal.className = "todolist__deal bg-info p-2 mt-2 rounded text-light shadow";

		if (animate != false){
			deal.classList.add("fadeInLeft");
		}
		
		if (item_add.check == "checked"){
			image_checkbox.src = this.image_checkbox_src_check;
			button_checkbox.className = "totolist__deal-checkbox checked mr-2";
		} else {
			image_checkbox.src = this.image_checkbox_src_uncheck;
			button_checkbox.className = "totolist__deal-checkbox unchecked mr-2";
		}
		
		button_checkbox.appendChild(image_checkbox);

        input_content.innerText = item_add.task;
        input_content.className = "totolist__deal-content";

		button_delete_deal.className = "todolist__deal-delete ml-auto";
		image_delete_deal.src = this.image_delete_deal_src;
		button_delete_deal.appendChild(image_delete_deal);

		//методы

		function alls() {
			function resss(resolve, reject){
				console.log(1);
				this.removeShadow();
				resolve();
			}

			let a = new Promise(resss.bind(this));

			a.then(()=>{
				console.log(2);
				return new Promise((resolve, reject)=>{
					event.stopPropagation();
					deal.classList.add("fadeOutRight");
					deal.addEventListener('animationend', x);
					function x(){
						deal.parentNode.removeChild(deal);
						resolve();
					}
					
					item_add.existance = false;
				});

			}).then(()=>{
				console.log(3);

				return new Promise((resolve, reject)=>{
					this.deleteDeals();
					resolve();
				});

			}).then(()=>{
				console.log(4);
				this.countUncheckDeals();
			});
		}

		button_delete_deal.addEventListener('click', alls.bind(this));

		deal.addEventListener("click", function(){
            deal.classList.toggle("red-boxshadow");
			deal.classList.toggle("shadow");
		});

		deal.addEventListener('click', this.resetToolbuttonsStyle.bind(this));

		button_checkbox.addEventListener('click', function(){
            this.classList.toggle("unchecked");
            this.classList.toggle("checked");
			if (this.classList.contains("checked")){
				this.firstChild.src = "dist\\img\\checked.svg";
				item_add.check = "checked";
			} else {
				this.firstChild.src = "dist\\img\\unchecked.svg";
				item_add.check = "unchecked";
			}
        });
        
		button_checkbox.addEventListener('click', this.buttonСheckboxOnclick.bind(this));
		
		//создание копи
		deal.appendChild(button_checkbox);
		deal.appendChild(input_content);
		deal.appendChild(button_delete_deal);
		this.$content_block.insertBefore(deal, this.$content_block.firstChild);
    }
    
    countUncheckDeals() {
        let i = 0, j = 0;

        //КОСТЫЛЬ

        while (this.$content_block.childNodes[i] && this.$content_block.childNodes[i].tagName) {
            if (this.$content_block.childNodes[i].firstChild.classList.contains("unchecked")) {
                j++;
            }
            i++;
        }
        this.$count_block.innerText = "Unchecked tasks: " + j;
    }

    buttonСheckboxOnclick() {
        this.resetToolbuttonsStyle();
        this.countUncheckDeals();
        event.stopPropagation();
        localStorage.setItem(this.idelem + "", JSON.stringify(this.deals_mass))
    }

    check_empty_block() {
		if (this.$content_block.firstChild == null) return 0;
	}

    checkActiveDeals() {
		let i = 0, answer = false;
		while (this.$content_block.childNodes[i] && this.$content_block.childNodes[i].tagName) {
			if (this.$content_block.childNodes[i].firstChild.classList.contains("unchecked")) {
				answer = true;
				break;
			}
			i++;
		}
		return answer;
    }
    
    checkCompleteDeals() {
		let i = 0, answer = false;
		while (this.$content_block.childNodes[i] && this.$content_block.childNodes[i].tagName) {
			if (this.$content_block.childNodes[i].firstChild.classList.contains("checked")) {
				answer = true;
				break;
			}
			i++;
		}
		return answer;
    }
    
    checkRedShadowElements() {
		let i = 0, answer = false;
		while (this.$content_block.childNodes[i] && this.$content_block.childNodes[i].tagName) {
			if (this.$content_block.childNodes[i].classList.contains("red-boxshadow")){
				answer = true;
				break;
			}
			i++;
		}
		return answer;
    }
    
    selectAllDeals() {
		if (this.check_empty_block() == 0){
			alert("Список дел пуст!");
			return 0;
		}
		this.removeShadow();
		this.changeStyleOnButtonAll();
		let i = 0;
		if (this.whatToolButtonIsClicked() == "All"){
			while (this.$content_block.childNodes[i] && this.$content_block.childNodes[i].tagName){
                this.$content_block.childNodes[i].classList.toggle("red-boxshadow");
                this.$content_block.childNodes[i].classList.toggle("shadow");
				i++;
			}
			
		} else {
			this.removeShadow();
		}
    }
    
    selectActiveDeals() {
		if (this.checkActiveDeals() == false || this.check_empty_block() == 0){
			alert("В вашем списке нету активных дел!");
			return 0;
		}
		this.removeShadow();
		this.changeStyleOnButtonActive();
		let i = 0;
		if (this.whatToolButtonIsClicked() == "Active"){
			while (this.$content_block.childNodes[i] && this.$content_block.childNodes[i].tagName) {
				if (this.$content_block.childNodes[i].firstChild.classList.contains("unchecked")) {
                    this.$content_block.childNodes[i].classList.add("red-boxshadow");
                    this.$content_block.childNodes[i].classList.remove("shadow");
                } else {
                    this.$content_block.childNodes[i].classList.add("shadow");
                    this.$content_block.childNodes[i].classList.remove("red-boxshadow");
                }
				i++;
			}
		} else {
			this.removeShadow();
		}
    }
    
    selectCompleteDeals() {
	    if (this.checkCompleteDeals() == false || this.check_empty_block() == 0){
	    	alert("В вашем списке нету выполненных дел!");
	    	return 0;
	    }
		this.removeShadow();
		this.changeStyleOnButtonComplete();
		let i = 0;
		if (this.whatToolButtonIsClicked() == "Complete"){
			while (this.$content_block.childNodes[i] && this.$content_block.childNodes[i].tagName) {
				if (this.$content_block.childNodes[i].firstChild.classList.contains("checked")) {
					this.$content_block.childNodes[i].classList.add("red-boxshadow");
                    this.$content_block.childNodes[i].classList.remove("shadow");
                } else {
                    this.$content_block.childNodes[i].classList.add("shadow");
                    this.$content_block.childNodes[i].classList.remove("red-boxshadow");
                }
				i++;
			}
		} else {
			this.removeShadow();
		}
    }
    
    whatToolButtonIsClicked() {
		if (this.$tool_block_button_all.classList.contains("selected-toolbutton")) return ("All");
		if (this.$tool_block_button_active.classList.contains("selected-toolbutton")) return ("Active");
		if (this.$tool_block_button_complete.classList.contains("selected-toolbutton")) return ("Complete");
    }
    
    clickOnButtonClearItems() {
		if (this.check_empty_block == 0){
			alert("Список дел пуст");
			return 0;
		}
		let j = this.whatToolButtonIsClicked();
		let i = 0;
		if (this.checkRedShadowElements()){
			if (confirm("Удалить выбранные дела?")){
				let len = this.deals_mass.length-1;
				while(this.$content_block.childNodes[i] && this.$content_block.childNodes[i].tagName) {
					if (this.$content_block.childNodes[i].classList.contains("red-boxshadow")) {
						//ставим признак удаления для конкретного дела в массиве
						this.deals_mass[len-i].existance = false;
						//анимация и последующее удаление дела
						this.$content_block.childNodes[i].classList.add("fadeOutRight");
						this.$content_block.childNodes[i].addEventListener('animationend', function() {
							this.remove();
						});
					}
					i++;
				}
				this.deleteDeals();
			}
		} else {
			alert("Выберите дела для удаления!");
		}
	}
	
	resetToolbuttonsStyle() {
		if(this.$tool_block_button_active.classList.contains("selected-toolbutton")){
			this.$tool_block_button_active.classList.toggle("selected-toolbutton");
			this.$tool_block_button_active.classList.toggle("unselected-toolbutton");
		}
		if(this.$tool_block_button_complete.classList.contains("selected-toolbutton")){
			this.$tool_block_button_complete.classList.toggle("selected-toolbutton");
			this.$tool_block_button_complete.classList.toggle("unselected-toolbutton");
		}
		if(this.$tool_block_button_all.classList.contains("selected-toolbutton")){
			this.$tool_block_button_all.classList.toggle("selected-toolbutton");
			this.$tool_block_button_all.classList.toggle("unselected-toolbutton");
		}
    }

    changeStyleOnButtonAll() {
		this.resetToolbuttonsStyle();
		this.$tool_block_button_all.classList.toggle("selected-toolbutton");
		this.$tool_block_button_all.classList.toggle("unselected-toolbutton");
		if (this.$tool_block_button_all.classList.contains("unselected-toolbutton")){
			this.removeShadow();
		}
	}

	//доделать
	
    changeStyleOnButtonActive() {
		this.resetToolbuttonsStyle();
		this.$tool_block_button_active.classList.toggle("selected-toolbutton");
		this.$tool_block_button_active.classList.toggle("unselected-toolbutton");
		if (this.$tool_block_button_active.classList.contains("unselected-toolbutton")){
			this.removeShadow();
		}
    }
    
    changeStyleOnButtonComplete() {
		this.resetToolbuttonsStyle();
		this.$tool_block_button_complete.classList.toggle("selected-toolbutton");
		this.$tool_block_button_complete.classList.toggle("unselected-toolbutton");
		if (this.$tool_block_button_complete.classList.contains("unselected-toolbutton")){
			this.removeShadow();
		}
	}

}

let id_element1 = document.getElementById("wrapper");
let id_element2 = document.getElementById("wrapper2");

let a = new CopyBlock(id_element1);
let b = new CopyBlock(id_element2);

//переделать кнопки toolblock